# Flex Table

`flex.table` is built on top of 
[data.table](https://github.com/Rdatatable/data.table) and allows to track, 
undo 
and redo operations that have been applied to data. Behind the scenes,
it uses [rlang](https://github.com/r-lib/rlang) to store expressions such 
that they can be executed
at any point in time.

`flex.table` provides a way
to track, undo and redo operations applied to datasets.
With an ordinary [data.table](https://github.com/Rdatatable/data.table), 
you cannot see which operations have been
applied to the data. Also, it is not possible to disable specific operations.
With `flex.table`, data pipelines become more transparent, easy to debug and
the datasets can be shared more easily, since all operations that have been
applied can be inspected.

To store these operations, `flex.table` uses so called layers. 
A layer is storing a quoted expression 
created using [rlang](https://github.com/r-lib/rlang), so it can be
evaluated at a later point in time.

## Features
* Small learning curve for [data.table](https://github.com/Rdatatable/data.table) users
* Track, undo reapply operations to datasets
* Facilitates sharing datasets since all operations can be inspected
* Makes debugging complex data transformations easier

## Usage
```r
data("iris")
mydt <- data.table::as.data.table(iris)
myft <- flex.table::as.flex.table(mydt)
```
Make some modifications to the dataset:
This operation is executed lazily. 
When creating this operation, it is not actually evaluated.
Only when we request the data, the expression will be evaluated.
```r
myft[, Sepal.Length := Sepal.Length / 2]
```
You can view the operation that has been applied to the dataset:
```r
myft$showLayers()
```
You can refer to function names that have been applied to the `flex.table`
and use those to disable all operations that were done by a function:
```r
winsorize <- function(myft, q_lower, q_upper, col_name) {
  lower_q <- stats::quantile(x = myft[[col_name]],
                             probs = q_lower, na.rm = TRUE)
  upper_q <- stats::quantile(x = myft[[col_name]],
                             probs = q_upper, na.rm = TRUE)
  myft[get(col_name) >= lower_q]
  myft[get(col_name) <= upper_q]
}
winsorize(myft = myft, 
          q_lower = 0.05, 
          q_upper = 0.95, 
          col_name = "Sepal.Length")
```          
When printing the layers of the `flex.table`, you will see
tat two layers have been created by the function 'winsorize'
these two layers correspond to the two expressions that have
been added to `flex.table`. You can now use the name of
the function to make all operations of it ineffective:
```r
myft$disable_by_name("winsorize")
```
Since we did not delete this layer, you can enable it again:
```r
myft$enable_by_name("winsorize")
```
You can also delete layers permanently:
```r
myft$delete_by_name("winsorize")
```

## Getting started
[Introduction to flex.table](doc/flextable-introduction.html) vignette
