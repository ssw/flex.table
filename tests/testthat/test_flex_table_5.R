testthat::test_that(
  desc = "2 subsets, variables used for columns are deleted from global environment",
  code =
    {
      n <- 10
      all <- data.table::data.table(c1 = 1:n)

      ft <- flex.table::as.flex.table(all)

      # train ----------------------------------------------------------
      n_train <- 6
      sft <- flex.table::create.sub.table(
        subset_name = "train",
        row_nums = 1:n_train,
        ft = ft
      )
      prediction <- stats::runif(n = n_train)
      prediction_col_name <- "prediction2"

      sft[, paste(prediction_col_name) := prediction]

      rm(prediction, prediction_col_name)
      dt_train <- sft$getDt()

      testthat::expect_equal(nrow(dt_train), n_train)
      testthat::expect_true("prediction2" %in% colnames(dt_train))

      # test ----------------------------------------------------------
      n_test <- 4
      sft2 <- flex.table::create.sub.table(
        subset_name = "test",
        row_nums = (n_train + 1):n,
        ft = ft
      )
      prediction <- stats::runif(n = n_test)
      prediction_col_name <- "prediction2"
      sft2[, prediction_col_name := prediction]
      rm(prediction, prediction_col_name)
      dt_test <- sft2$getDt()

      testthat::expect_equal(nrow(dt_test), n_test)
      testthat::expect_true("prediction2" %in% colnames(dt_test))

      # all ----------------------------------------------------------
      dt_all <- ft$getDt()

      testthat::expect_equal(nrow(dt_all), n)

      testthat::expect_equal(
        object = sort(dt_all$prediction2),
        expected = sort(c(dt_train$prediction2, dt_test$prediction2))
      )
    }
)


testthat::test_that(
  desc = "a flex table, variables used for columns are deleted from global environment",
  code =
    {
      n <- 5e5
      all <- data.table::data.table(c1 = 1:n, fillsize = 1:n)

      tmp <- rbenchmark::benchmark(
        "data.table" =
          {
            size_col_name <- "fillsize"
            all2 <- data.table::copy(all)
            all2[, paste(size_col_name) := 1 / c1 * get(size_col_name)]
          },
        "flex.table" =
          {
            FT <- flex.table::as.flex.table(all)
            size_col_name <- "fillsize"
            FT[, paste(size_col_name) := 1 / c1 * get(size_col_name)]
            rm(size_col_name)
            dummy <- !FT
          },
        replications = 100,
        columns = c("test", "replications", "elapsed")
      )

      tmp <- tmp %>% data.table::as.data.table()

      cli::cat_line("\ndata.table: ", tmp[test == "data.table"]$elapsed, " s")
      cli::cat_line("flex.table: ", tmp[test == "flex.table"]$elapsed, " s")

      # note on 2020-11-19
      # data.table: 0.674999999999272 s
      # flex.table: 9.89899999999761 s
      # will change factor to 15
      factor <- 15

      testthat::expect_lt(
        !!tmp[test == "flex.table"]$elapsed,
        !!(factor * tmp[test == "data.table"]$elapsed)
      )
    }
)
