testthat::test_that(
  desc = "getLayersDt: no layers",
  code =
    {
      dt <- data.table::data.table()
      FT <- flex.table::as.flex.table(dt)

      testthat::expect_message(
        object = {layers_dt <- FT$getLayersDt()},
        regexp = "no layers"
      )
      testthat::expect_equal(layers_dt, data.table::data.table())
    }
)

testthat::test_that(
  desc = "getLayersDt: some layers, no obj_name",
  code =
    {
      dt <- data.table::data.table(
        trade_id = 1000:1002,
        volume = c(97, 43, 38),
        instrument = c("I1", "I2", "I1"),
        price = c(22.1, 19.4, 21.5)
      )
      FT <- flex.table::as.flex.table(dt)
      FT[, just_a_column := 42]
      FT[, price := 2 * price]

      some_size <- 1:3
      FT[, size := some_size]
      FT$disableLayers(2)

      layers_dt <- FT$getLayersDt()

      expected_dt <- data.table::data.table(
        layer_id = 1:3,
        text_code = c(
          "dt[^, ^just_a_column := 42]",
          "dt[^, ^price := 2 * price]",
          "dt[^, ^size := some_size]"
        ),
        enabled = c(TRUE, FALSE, TRUE),
        obj_name = rep(NA_character_, 3),
        obj_class = rep(NA_character_, 3)
      )

      testthat::expect_equal(layers_dt, expected_dt)
    }
)
