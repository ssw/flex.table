# SubFlexTable ####
SubFlexTable <- R6::R6Class(
  classname = "SubFlexTable",
  inherit = NULL

  # active #####
  , active = list(

    # Active binding for accessing parent table autoBake
    autoBake = function(bake_flag)
    {
      if (base::missing(bake_flag))
      {
        return(private$.flexTable$autoBake)
      }

      private$.flexTable$autoBake <- bake_flag
    }

    # Active binding for accessing parent table autoCache
    , autoCache = function(cache_flag)
    {
      if (base::missing(cache_flag))
      {
        return(private$.flexTable$autoCache)
      }

      private$.flexTable$autoCache <- cache_flag
    }
  )

  # public #####
  , public = list(
    initialize = function(
      subset_name,
      row_nums,
      dataset_name,
      flexTable,
      obj_name,
      obj_class
    ) {
      checkmate::assertTRUE("FlexTable" %in% class(flexTable))

      checkmate::assertString(
        x = subset_name,
        na.ok = FALSE,
        null.ok = FALSE
      )
      checkmate::assertNumeric(
        x = row_nums,
        min.len = 1,
        any.missing = FALSE,
        unique = TRUE,
        lower = 1
      )
      private$.flexTable <- flexTable
      private$.flag_name <- paste0("is_", subset_name, "_", dataset_name)
      # to recreate a subset which might be different, remove column
      # indicating which rows belong to that subset in the flextable
      if (private$.flag_name %in% colnames(flexTable$getDt()))
      {
        flexTable[, (private$.flag_name) := NULL]
      }
      private$.row_nums <- row_nums
      private$.subset_name <- subset_name

      # note: it looks like the version with eval is on average faster than the
      # using bang bang operator
      # self$addLayer(, !!private$.flag_name := TRUE)
      #

      tmp_text <- glue_null(
        "self$addLayer(, {private$.flag_name} := TRUE, ",
        "obj_name = '{obj_name}', obj_class = '{obj_class}'",
        ")"
      )
      eval(parse(text = tmp_text))
    }

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    , showLayers = function(with_log = FALSE, max_trace_lines = NULL, layer_id = NULL)
    {
      private$.flexTable$showLayers(
        with_log = with_log,
        max_trace_lines = max_trace_lines,
        layer_id = layer_id
      )
    }

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    , getLayersDt = function()
    {
      private$.flexTable$getLayersDt()
    }

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    , disableLayers = function(indices)
    {
      private$.flexTable$disableLayers(indices)
    }

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    , deleteLayers = function(indices)
    {
      private$.flexTable$deleteLayers(indices)
    }

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    , enableLayers = function(indices)
    {
      private$.flexTable$enableLayers(indices)
    }

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Subsets: if we have for example train[, new_column := c(1,3)],
    # where train is a subset (i.e. has less rows than all),
    # adding the layer fails since we try to add a vector with 2 elements to
    # a data.table with more than 2 rows.
    # So we first withCallingHandlers, and then try to deal with the error.
    #
    , addLayer = function(
      ...,
      obj_name,
      obj_class,
      fix_rows = FALSE
    ) {
      qq0 <- rlang::enquos(
        ...,
        .unquote_names = FALSE,
        .homonyms = "error",
        .check_assign = TRUE,
        .named = FALSE
      )

      local_name <- paste0(private$.subset_name, "_row_nums__")

      empty_i <- rlang::quo_get_expr(qq0[[1]]) == ""

      if (empty_i)
      {
        qq0[[1]] <- rlang::quo_set_expr(
          quo = qq0[[1]],
          expr = rlang::parse_expr(glue::glue("row_id_ %in% {local_name}"))
        )
      }
      else
      {
        local_text <- "row_id_ %in% {local_name} & {rlang::expr_text(rlang::quo_get_expr(qq0[[1]]))}"

        qq0[[1]] <- rlang::quo_set_expr(
          quo = qq0[[1]],
          expr = rlang::parse_expr(glue::glue(local_text))
        )
      }

      local_env <- rlang::quo_get_env(qq0[[1]])

      if (rlang::is_empty(local_env))
      {
        local_env <- rlang::env()
      }
      assign(x = local_name, value = private$.row_nums, envir = local_env)

      qq0[[1]] <- rlang::quo_set_env(
        quo = qq0[[1]],
        env = local_env
      )

      layer_id <- private$.flexTable$addLayer(
        !!!qq0,
        subset_flag = private$.flag_name,
        external_method = NULL,
        obj_name = obj_name,
        obj_class = obj_class,
        fix_rows = fix_rows
      )

      return(invisible(layer_id))
    }

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    , getDt = function()
    {
      subset_is_enabled <-
        private$.flag_name %in% private$.flexTable$getNames()

      if (!subset_is_enabled)
      {
        warning("Subset ", private$.flag_name, ": no layer (enabled)")
        return(data.table::data.table())
      }

      tmp <- private$.flexTable$getDt()
      tmp2 <- tmp[get(private$.flag_name) == TRUE]
      tmp2[, (private$.flag_name) := NULL]

      return(tmp2)
    },

    getColumns = function(parameterNames)
    {
      subset_is_enabled <-
        private$.flag_name %in% private$.flexTable$getNames()

      if (!subset_is_enabled)
      {
        warning("Subset ", private$.flag_name, ": no layer (enabled)")
        return(data.table::data.table())
      }

      return(
        private$.flexTable$getColumns(parameterNames,
                                      subsetSelector = private$.flag_name
        )
      )
    },

    getRowCount = function()
    {
      return(private$.flexTable$getRowCount(
        subsetSelector = private$.flag_name
      ))
    },

    getNames = function()
    {
      return(private$.flexTable$getNames())
    },

    createSubset = function(
      subset_name,
      row_nums,
      dataset_name,
      obj_name,
      obj_class
    ) {
      subset <- SubFlexTable$new(
        subset_name = subset_name,
        row_nums = row_nums,
        dataset_name = dataset_name,
        flexTable = private$.flexTable,
        obj_name = obj_name,
        obj_class = obj_class
      )
      return(subset)
    }
  )

  # private #####
  , private = list(
    .flexTable = NULL,
    .flag_name = NULL,
    .row_nums = NULL,
    .subset_name = NULL
  )
)


#' @export
#' @method `[` SubFlexTable
#'
`[.SubFlexTable` <- function(sft, ..., .FIX_ROWS = FALSE)
{
  caller_info <- .get_caller_info_list()

  sft$addLayer(
    ...,
    obj_name = caller_info$name,
    obj_class = caller_info$class,
    fix_rows = .FIX_ROWS
  )
}

#' @export
#' @method `[[` SubFlexTable
#'
`[[.SubFlexTable` <- function(sft, parameter_name)
{
  # Extract a set of columns from cache without copying them
  dt <- sft$getColumns(parameter_name)

  return(invisible(dt))
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#' nrow
#'
#' @param ft \code{SubFlexTable}
#'
#' @export
nrow <- function(ft)
{
  UseMethod("nrow")
}

#' @export
nrow.SubFlexTable <- function(ft)
{
  return(ft$getRowCount())
}

#' @export
nrow.default <- function(ft)
{
  return(base::nrow(ft))
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#' names
#'
#' @param x \code{SubFlexTable}
#' @export
names.SubFlexTable <- function(x)
{
  return(x$getNames())
}
