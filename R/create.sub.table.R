#' create.sub.table
#'
#' @param subset_name subset_name
#' @param row_nums row_nums
#' @param ft ft
#'
#' @export
create.sub.table <- function(subset_name = "SubFlextable", row_nums, ft)
{
  checkmate::assertNumeric(
    x = row_nums,
    any.missing = FALSE,
    min.len = 1,
    lower = 1
  )
  cat(paste("\nCreating subset", subset_name))

  obj_info <- .get_caller_info_list()

  sft <- ft$createSubset(
    subset_name = subset_name,
    row_nums = row_nums,
    dataset_name = ft$name,
    obj_name = obj_info$name,
    obj_class = obj_info$class
  )

  # do not call getDt() here, since it would apply all the layers,
  # which might be slow
  cat(paste(
    "\nsubset", subset_name, "has", length(row_nums), "rows."
  ))
  return(sft)
}
